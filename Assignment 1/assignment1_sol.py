import math
g = 9.81
pi = math.pi

def get_distance(velocity, angle):
    return (velocity**2 * math.sin(2*angle) / g)

def degrees_to_radians(d):
    return d * (pi/180)

def get_radian_of(angle_string):
    if angle_string[-1] in "rR":
        return float(angle_string[1:-1])
    return degrees_to_radians(float(angle_string[:-1]))

def is_a_number(s):
    if "." in s:
        cond1 = s[:s.find(".")].isnumeric()
        cond2 = (s[s.find(".")+1:].isnumeric() or s[s.find(".")+1:] == "")
        return  cond1 and cond2 
    return s.isnumeric()

def is_valid_angle(s):
    if not is_a_number(s[:-1]):
        return False
    if s[-1] in "dD":
        num = float(s[:-1])
        return num > 0 and num < 90 
    if s[-1] in "rR":
        num = float(s[:-1])
        return num > 0 and num < pi/2
    return False

def approx_equal(x, y, tol):
    return abs(x-y) <= tol

if __name__ == "__main__":
    while True:
        target = float(input("Enter a target distance: "))
        target_hit = False
        while not target_hit:
            valid_velocity = False
            while not valid_velocity:
                v = input("Enter a valid velocity: ")
                valid_velocity = is_a_number(v)   
            valid_angle = False
            v = float(v)
            while not valid_angle:
                theta = input("Enter a valid angle: ")
                valid_angle = is_valid_angle(theta)
            theta = get_radian_of(theta)
            d = get_distance(float(v), theta)
            tol = 1
            target_hit = approx_equal(target, d, tol)
            if target_hit:
                print("Congratulations! You hit the target.")
            elif target > d:
                print("The shot hit short of the target, try again.")
            else: 
                print("The shot hit past the target, try again.")
            
                
